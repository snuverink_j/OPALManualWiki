\input{header}

\chapter{Control Statements}
\label{chp:control}
\index{Control Statements|(}
\index{Program Control}

\section{Getting Help}

\subsection{HELP Command}
\label{sec:help}
\index{HELP}
A user who is uncertain about the attributes of a command
should try the command \texttt{HELP}, which has three formats:
\begin{verbatim}
HELP;                 // Give help on the "HELP" command
HELP,NAME=label;      // List funct. and attr. types of
                      // "label"
HELP,label;           // Shortcut for the second format
\end{verbatim}
\texttt{label} is an {identifier} see~Section~\ref{label}.
If it is non-blank,
\textit{OPAL} prints the function of the object \texttt{label} and lists its
attribute types.
Entering \texttt{HELP} alone displays help on the \texttt{HELP}
command.

\noindent Examples:
\begin{verbatim}
HELP;
HELP,NAME=TWISS;
HELP,TWISS;
\end{verbatim}

\subsection{SHOW Command}
\label{sec:show}
\index{SHOW}
The \texttt{SHOW} statement displays the current attribute values
of an object.
It has three formats:
\begin{verbatim}
SHOW;                 // Give help on the "SHOW" command
SHOW,NAME=pattern;    // Show names matching of "pattern"
SHOW,pattern;         // Shortcut for the second format
\end{verbatim}
\texttt{pattern} is an {regular expression} see~Section~\ref{wildcard}.
If it is non-blank,
\textit{OPAL} displays all object names matching the \texttt{pattern}.
Entering \texttt{SHOW} alone displays help on the \texttt{SHOW}
command.

\noindent Examples:
\begin{verbatim}
SHOW;
SHOW,NAME="QD.*\.L*";
SHOW,"QD.*\.L*";
\end{verbatim}

\subsection{WHAT Command}
\label{sec:what}
\index{WHAT}
The \texttt{WHAT} statement displays all object names matching a given
regular expression.
It has three formats:
\begin{verbatim}
WHAT;                   // Give help on the "WHAT" command
WHAT,NAME=label;        // Show definition of "label"
WHAT,label;             // Shortcut for the second format
\end{verbatim}
\texttt{label} is an {identifier} see~Section~\ref{label}.
If it is non-blank,
\textit{OPAL} displays the object \texttt{label} in a format similar to the
input statement that created the object.
Entering \texttt{WHAT} alone displays help on the \texttt{WHAT}
command.
\noindent Examples:
\begin{verbatim}
WHAT;
WHAT,NAME=QD;
WHAT,QD;
\end{verbatim}

\section{STOP / QUIT Statement}
\label{sec:stop}
\index{STOP / QUIT}
The statement
\begin{verbatim}
STOP or QUIT;
\end{verbatim}
terminates execution of the \textit{OPAL} program,
or, when the statement occurs in a \texttt{CALL} file see~Section~\ref{call},
returns to the calling file.
Any statement following the \texttt{STOP} or  \texttt{QUIT} statement is ignored.

\section{OPTION Statement}
\label{sec:option}
\index{OPTION}
The \texttt{OPTION} command controls global command execution and sets
a few global quantities. Starting with version 1.6.0 of \textit{OPAL} the option \texttt{VERSION} is
mandatory in the \textit{OPAL} input file. Example:
\begin{verbatim}
OPTION,ECHO=logical,INFO=logical,TRACE=logical,
       WARN=logical,
       SEED=real,PSDUMPFREQ=integer,
       STATDUMPFREQ=integer, SPTDUMFREQ=integer,
       REPARTFREQ=integer, REBINFREQ=integer, TELL=logical, VERSION=integer;
\end{verbatim}

\begin{description}
\item[VERSION]
  \index{VERSION}

  Used to indicate for which version of \textit{OPAL} the input file is
  written. The major and minor versions of \textit{OPAL} and of the input file
  have to match. The patch version of \textit{OPAL} must be greater or equal
  to the patch version of the input file. If the version doesn't
  fulfill above criteria \textit{OPAL} stops immediately and prints
  instructions on how to convert the input file.  The format is
  \texttt{Mmmpp} where \texttt{M} stands for the major, \texttt{m} for
  the minor and \texttt{p} for the patch version. For version 1.6.0 of
  \textit{OPAL} \texttt{VERSION} should read \texttt{10600}.
\end{description}

The next five logical flags activate or deactivate execution options:
\begin{description}
  \item[ECHO]
  \index{OPTION!ECHO}
  Controls printing of an echo of input lines on the standard error file.

  \item[INFO]
  \index{OPTION!INFO}
  If this option is turned off, \textit{OPAL} suppresses all information messages. It also affects the \textit{gnu.out} and \textit{eb.out}  files in case of \textit{OPAL-cycl} simulations.

  %% \item[TRACE]
  %%   \index{TRACE}
  %%   When the \texttt{TRACE} option is on,
  %%   \textit{OPAL} writes additional trace information on the standard error file
  %%   for each executable command.
  %%   This information includes the command name
  %%   and elapsed CPU time before and after the command.
  \item[WARN]
  \index{OPTION!WARN}
  If this option is turned off, \textit{OPAL} suppresses all warning messages.

  \item[TELL]
  \index{OPTION!TELL}
  If true, the current settings are listed. Must be the last option in the inputfile in order to render correct results.

  \item[SEED]
  \index{OPTION!SEED}
  Selects a particular sequence of random values.
  A SEED value is an integer in the range [0...999999999] (default: 123456789).
  SEED can be an expression. If SEED $=$ -1, the time is used as seed and the generator is not portable anymore.
  See also: random values see~Section~\ref{adefer}.

  \item[PSDUMPFREQ]
  \index{OPTION!PSDUMPFREQ}
  Defines after how many time steps the phase space is dumped into the H5hut file. Default value is 10.

  \item[STATDUMPFREQ]
  \index{OPTION!STATDUMPFREQ}
  Defines after how many time steps we dump statistical data, such as RMS beam emittance, to the .stat file.
  The default value is 10. Currently only available for \textit{OPAL-t}.

  \item[SPTDUMPFREQ]
  \index{OPTION!SPTDUMPFREQ}
  Defines after how many time steps we dump the phase space of single particle.
  It is always useful to record the trajectory of reference particle
  or some specified particle for primary study. Its default value is 1.

  \item[REPARTFREQ]
  \index{OPTION!REPARTFREQ}
  Defines after how many time steps we do particles repartition to balance the computational load of
  the computer nodes. Its default value is 10.

  \item[REBINFREQ]
  \index{OPTION!REBINFREQ}
  Defines after how many time steps we update the energy Bin ID of each particle. For the time being.
  Only available for multi-bunch simulation in \textit{OPAL-cycl}. Its default value is 100.

  \item[PSDUMPEACHTURN]
  \index{OPTION!PSDUMPEACHTURN}
  Control option of phase space dumping. If true, dump phase space after each turn.
  For the time being, this is only use for multi-bunch simulation in \textit{OPAL-cycl}. Its default set is false.

  \item[PSDUMPFRAME]
  \index{OPTION!PSDUMPFRAME}
  Control option that defines the frame in which the phase space data is written for h5 and stat files.
    \begin{itemize}
      \item \verb|GLOBAL|: data is written in the global Cartesian frame;
      \item \verb|BUNCH_MEAN|: data is written in the bunch mean frame or;
      \item \verb|REFERENCE|: data is written in the frame of the reference particle.
    \end{itemize}

  \item[SCSOLVEFREQ]
  \index{OPTION!SCSOLVEFREQ}
  If the space charge field is slowly varying w.r.t. external fields,  this option allows to change the frequency of space charge calculation,
  i.e. the space charge forces are evaluated every SCSOLVEFREQ step and then reused for the following steps. Affects integrators LF-2 and RK-4 of \textit{OPAL-cycl}. Its default value is 1. Note: as the multiple-time-stepping (MTS) integrator maintains accuracy much better with reduced space charge solve frequency, this option should probably not be used anymore.

  \item[MTSSUBSTEPS]
  \index{OPTION!MTSSUBSTEPS}
  Only used for multiple-time-stepping (MTS) integrator in \textit{OPAL-cycl}. Specifies how many sub-steps for external field integration are done per step. Default value is 1.
  Making less steps per turn and increasing this value is the recommended way to reduce space charge solve frequency.

  \item[RHODUMP]
  \index{OPTION!RHODUMP}
  If true the scalar $\rho$ field is saved each time a phase space is written. There exists a reader in Visit with versions
  greater or equal 1.11.1.

  \item[EFDUMP]
  \index{OPTION!EFDUMP}
  Not supported anymore.

  \item[EBDUMP]
  \index{OPTION!EBDUMP}
  If true the electric and magnetic field on the particle is saved each time a phase space is written.

  \item[CSRDUMP]
  \index{OPTION!CSRDUMP}
  If true the electric csr field component, $E_z$, line density and the derivative of the line density is written into the \textit{data} directory.
  The first line gives the average position of the beam bunch.
  Subsequent lines list $z$ position of longitudinal mesh (with respect to the head of the beam bunch),
   $E_z$, line density and the derivative of the line density. Note that currently the line density derivative
   needs to be scaled by the inverse of the mesh spacing to get the correct value. The CSR field
   is dumped at each time step of the calculation. Each text file is named "Bend Name" (from input file) + "-CSRWake" + "time step number in that bend
   (starting from 1)" + ".txt".

  %% \item[AUTOPHASE]
  %% \index{OPTION!AUTOPHASE}
  %% A phase scan of all $n$ RF-elements is performed, if  \texttt{AUTOPHASE} is greater than zero.   \texttt{AUTOPHASE} finds the maximum energy
  %% in a way similar to the code Astra.
  %% \begin{enumerate}
  %%   \item  find the phase $\phi_i$ for maximum energy of the $i$\engordletters{th} cavity.
  %%   \item  track is continued with $\phi_i + LAG$ to the element $i+1$.
  %%   \item  if $i<n$ goto $1$
  %% \end{enumerate}
  %% For convenience a file (\textit{inputfn.phases}) with the phases corresponding to the maximum energies is written. A \texttt{AUTOPHASE} value of $4$ gives Astra comparable results.
  %% An example is given in see~Section~\ref{trackautoph}.

  %The range of the phase within which the energy is maximized is refined \texttt{AUTOPHASE} times. For each level of refinement, $i$, the energy is evaluated at $11$ evenly distributed  positions between $\phi_{i, \, \mathrm{low}}$ and $\phi_{i, \, \mathrm{high}}$ with spacing $\mathrm{d}\phi_i = \frac{\phi_{i, \, \mathrm{high}} - \phi_{i, \, \mathrm{low}}}{10}$ such that $\phi_{i,j} = \phi_{i, \, \mathrm{low}} + j \cdot \mathrm{d}\phi_i \quad \forall j \in [0 \ldots 10]$. The new search range is then defined by $\phi_{i,\,\mathrm{max}} \pm \mathrm{d}\phi_i$. The final phase after $N$ refinements has an accuracy of $\frac{2\pi}{5^{N}}$ where $N \equiv \mathrm{AUTOPHASE}$.
  %The higher \texttt{AUTOPHASE} is the more accurate the result but also the more time is needed to compute the result.

  \item[SCAN]
  \index{OPTION!SCAN}
  If true one can simulate in a loop several machines where some variables can be random variables. Find an
  example in Section~\ref{randmach}.

  \item[CZERO]
  \index{OPTION!CZERO}
  If true the distributions are generated such that the centroid is exactly zero and not statistically dependent.

  \item[RNGTYPE]
  \index{OPTION!RNGTYPE}
  The name see~Section~\ref{astring} of a random number generator can be provided. The default random number generator (RANDOM) is a portable 48-bit generator. Three quasi random generators are available:
  \begin{enumerate}
    \item HALTON
    \item SOBOL
    \item NIEDERREITER.
  \end{enumerate}
  For details see the GSL reference manual (18.5).

  \item[ENABLEHDF5]
  \index{OPTION!ENABLEHDF5}
  If true (default), HDF5 read and write is enabled.

  \item[ASCIIDUMP]
  \index{OPTION!ASCIIDUMP}
  If true, instead of HDF5, ASCII output is generated for the following elements: Probe, Collimator, Monitor, Stripper, Foil and global losses.

  \item[NLHS]
  \index{OPTION!NLHS}
  Number of stored old solutions for extrapolating the new starting vector. Default value is 1 and just the last solution is used.

  \item[NUMBLOCKS]
  \index{OPTION!NUMBLOCKS}
  Maximum number of vectors in the Krylov space (for RCGSolMgr). Default value is 0 and BlockCGSolMgr will be used.

  \item[RECYCLEBLOCKS]
  \index{OPTION!RECYCLEBLOCKS}
  Number of vectors in the recycle space (for RCGSolMgr). Default value is 0 and BlockCGSolMgr will be used.

  \item[BOUNDPDESTROYFQ]
  \index{OPTION!BOUNDPDESTROYFQ}
  The frequency to do \texttt{boundp\_destroy} to delete lost particles. Default 10

  \item[REMOTEPARTDEL]
  \index{OPTION!REMOTEPARTDEL}
 \sloppy Artificially delete the remote particle if its distance to the beam mass is larger than \texttt{REMOTEPARTDEL} times of the beam rms size, its default values is -1 (no delete)

  \item[IDEALIZED]
  \index{OPTION!IDEALIZE}
  Instructs to use the hard edge model for the calculation of the path length in \textit{OPAL-t}. The path length is computed to place the elements in the three-dimensional space from \texttt{ELEMEDGE}. Default is false.

  \item[LOGBENDTRAJECTORY]
  \index{OPTION!LOGBENDTRAJECTORY}
  Save the reference trajectory inside dipoles in an ASCII file. For each dipole a separate file is written to the directory \textit{data/}. Default is false.

  \item[VERSION]
  \index{OPTION!VERSION}
  Used to indicate for which version of \textit{OPAL} the input file is written. The versions of \textit{OPAL} and of the input file have to match. The format is \texttt{Mmmpp} where \texttt{M} stands for the major, \texttt{m} for the minor and \texttt{p} for the patch version. For version 1.6.0 of \textit{OPAL} \texttt{VERSION} should read \texttt{10600}. If the version doesn't match then \textit{OPAL} stops immediately and prints instructions on how to convert the input file.

  \item[PPDEBUG]
  \index{OPTION!PPDEBUG}
  \TODO{Describe option PPDEBUG}

  \item[SURFDUMPFREQ]
  \index{OPTION!SURFDUMPFREQ}
  \TODO{Describe option SURFDUMPFREQ}

  \item[BEAMHALOBOUNDARY]
  \index{OPTION!BEAMHALOBOUNDARY}
  \TODO{Describe option BEAMHALOBOUNDARY}

  \item[CLOTUNEONLY]
  \index{OPTION!CLOTUNEONLY}
  \TODO{Describe option CLOTUNEONLY}
  
  \item[AMR]
  \index{AMR}
  Enable adaptive mesh refinement.\ Default: FALSE

  \item[MEMORYDUMP]
  \index{MEMORYDUMP}
  If true, it writes the memory consumption of every core to a SDDS file (*.mem).\ The write frequency corresponds to
  STATDUMPFREQ.\ Default: FALSE
\end{description}


\noindent Examples:
\begin{verbatim}
OPTION,ECHO=FALSE,TELL;
OPTION,SEED=987456321
\end{verbatim}

\begin{table}[ht] \footnotesize
  \caption{Default Settings for Options}
  \label{tab:option}
  \begin{center}
    \begin{tabular}{|ll|ll|ll|}
      \hline
         \texttt{ECHO}              & = \texttt{FALSE}     &
         \texttt{INFO}              & = \texttt{TRUE}      &
         \texttt{TRACE}             & = \texttt{FALSE}     \\
      \hline
         \texttt{WARN}              & = \texttt{TRUE}      &
         \texttt{SEED}              & = \texttt{123456789}  &
         \texttt{TELL}              & = \texttt{FALSE}     \\
      \hline
         \texttt{SPTDUMPFREQ}       & = \texttt{1}          &
         & &
         & \\
      \hline
         \texttt{PSDUMPFREQ}        & = \texttt{10}         &
         \texttt{STATDUMPFREQ}      & = \texttt{10}         &
         \texttt{REPARTFREQ}        & = \texttt{10}         \\
      \hline
         \texttt{BOUNDPDESTROYFQ}   & = \texttt{10}         &
         \texttt{REBINFREQ}         & = \texttt{100}        &
         \texttt{SCSOLVEFREQ}       & = \texttt{1}          \\
      \hline
         \texttt{PSDUMPEACHTURN}    & = \texttt{FALSE}     &
         \texttt{PSDUMPLOCALFRAME}  & = \texttt{FALSE}     &
         \texttt{MTSSUBSTEPS}       & = \texttt{1}          \\
      \hline
         \texttt{RHODUMP}           & = \texttt{FALSE}     &
         \texttt{EBDUMP}            & = \texttt{FALSE}     &
         \texttt{CSRDUMP}           & = \texttt{FALSE}     \\
      \hline
         \texttt{ASCIIDUMP}         & = \texttt{FALSE}     &
         \texttt{CZERO}             & = \texttt{FALSE}     &
         \texttt{RNGTYPE}           & = \texttt{"RANDOM"} \\
      \hline
         \texttt{SCAN}              & = \texttt{FALSE}     &
         \texttt{NUMBLOCKS}         & = \texttt{0}          &
         \texttt{RECYCLEBLOCKS}     & = \texttt{0}          \\
      \hline
         \texttt{NLHS}              & = \texttt{1}          &
         \texttt{IDEALIZE}          & = \texttt{FALSE}     &
         \texttt{ENABLEHDF5}        & = \texttt{TRUE}      \\
      \hline
         \texttt{REMOTEPARTDEL}     & = \texttt{-1}         &
         \texttt{LOGBENDTRAJECTORY} & = \texttt{FALSE}     &
         \texttt{PPDEBUG}           & = \texttt{FALSE}     \\
      \hline
         \texttt{SURFDUMPFREQ}      & = \texttt{-1}         &
         \texttt{BEAMHALOBOUNDARY}  & = \texttt{0.0}        &
         \texttt{CLOTUNEONLY}       & = \texttt{FALSE}     \\
      \hline
         \texttt{VERSION}           & = \texttt{none}       &
         \texttt{AMR}               & = \texttt{FALSE}      &
         \texttt{MEMORYDUMP}        & = \texttt{FALSE}      \\
      \hline
    \end{tabular}
  \end{center}
\end{table}

\section{Parameter Statements}
\label{sec:parameter}
\index{Parameters}

\subsection{Variable Definitions}
\label{sec:variable}
\index{Variable}
\textit{OPAL} recognizes several types of variables.

\subsubsection{Real Scalar Variables}
\index{Variable!Real}
\index{Real!Variable}
\begin{verbatim}
REAL variable-name=real-expression;
\end{verbatim}
For backward compatibility the program also accepts the form
\begin{verbatim}
REAL variable-name:=real-expression;
\end{verbatim}
This statement creates a new global variable \texttt{variable-name}
and discards any old variable with the same name.
Its value depends on all quantities occurring
in {real-expression} see~Section~\ref{areal}.
Whenever an operand changes in \texttt{real-expression},
a new value is calculated.
The definition may be thought of as a mathematical equation.
However, \textit{OPAL} is not able to solve the equation for a quantity on the
right-hand side.

An assignment in the sense of the FORTRAN or C languages can be achieved
by using the \texttt{EVAL} function see~Section~\ref{eval}.

A reserved variable is the value \texttt{P0} which is used as the
global reference momentum for normalizing all magnetic field coefficients.
\noindent Example:
\begin{verbatim}
REAL GEV=100;
P0=GEV;
\end{verbatim}
Circular definitions are not allowed:
\begin{verbatim}
X=X+1;    // X cannot be equal to X+1
REAL A=B;
REAL B=A;      // A and B are equal, but of unknown value
\end{verbatim}
However, redefinitions by assignment are allowed:
\begin{verbatim}
X=EVAL(X+1);
\end{verbatim}

\subsubsection{Real Vector Variables}
\index{Real!Vector}
\index{Variable!Vector}
\index{Vector!Real}
\begin{verbatim}
REAL VECTOR variable-name=vector-expression;
\end{verbatim}
In old version of \textit{OPAL} (before 1.6.0) the keyword \texttt{REAL} was
optional, now it is mandatory!

This statement creates a new global variable \texttt{variable-name}
and discards any old variable with the same name.
Its value depends on all quantities occurring
in {vector-expression} see~Section~\ref{anarray} on the right-hand side.
Whenever an operand changes in \texttt{vector-expression},
a new value is calculated.
The definition may be thought of as a mathematical equation.
However, \textit{OPAL} is not able to solve the equation for a quantity on the
right-hand side.

\noindent Example:
\begin{verbatim}
REAL VECTOR A = TABLE(10, #);
REAL VECTOR B = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
\end{verbatim}
Circular definitions are not allowed, but redefinitions by assignment
are allowed.

\subsubsection{Logical Variables}
\index{Variable!Logical}
\index{Logical!Variable}
\begin{verbatim}
BOOL variable-name=logical-expression;
\end{verbatim}
This statement creates a new global variable \texttt{variable-name}
and discards any old variable with the same name.
Its value depends on all quantities occurring
in {logical-expression} see~Section~\ref{alogical}.
Whenever an operand changes in \texttt{logical-expression},
a new value is calculated.
The definition may be thought of as a mathematical equation.
However, \textit{OPAL} is not able to solve the equation for a quantity on the
right-hand side.

\noindent Example:
\begin{verbatim}
BOOL FLAG = X != 0;
\end{verbatim}
Circular definitions are not allowed, but redefinitions by assignment
are allowed.

\subsection{Symbolic Constants}
\label{sec:constant}
\index{Constants!Real}
\index{Real!Constant}
\textit{OPAL} recognizes a few build-in built-in mathematical and physical
constants see~Table~\ref{constant}.
Additional constants can be defined by the command
\begin{verbatim}
REAL CONST label:CONSTANT=<real-expression>;
\end{verbatim}
which defines a constant with the name \texttt{label}.
The keyword \texttt{REAL} is optional, and \texttt{label} must be unique.
An existing symbolic constant can never be redefined.
The \texttt{real-expression} is evaluated at the time the
\texttt{CONST} definition is read, and the result is stored as the
value of the constant.

\noindent Example:
\begin{verbatim}
CONST IN=0.0254; // conversion of inches to meters
\end{verbatim}

\subsection{Vector Values}
\label{sec:vector}
\index{Vector!Real}
\index{Real!Vector}
A vector of expressions is established by a statement
\begin{verbatim}
REAL VECTOR vector-name=vector-expression;
\end{verbatim}
The keyword \texttt{REAL} is optional.
It creates a new global vector \texttt{vector-name}
and discards any old vector with the same name.
Its value depends on all quantities occurring in
{vector-expression} see~Section~\ref{anarray}.
Whenever an operand changes in \texttt{vector-expression},
a new value is calculated.
The definition may be thought of as a mathematical equation.
However, \textit{OPAL} is not able to solve the equation for a quantity on the
right-hand side.

\noindent Example:
\begin{verbatim}
VECTOR A_AMPL={2.5e-3,3.4e-2,0,4.5e-8};
VECTOR A_ON=TABLE(10,1);
\end{verbatim}
Circular definitions are not allowed.

\subsection{Assignment to Variables}
\label{sec:eval}
\index{Variable!Assignment}
\index{Assignment}
A value is assigned to a variable or vector by using the function
\texttt{EVAL(real- expression)}.
When seen, this function is immediately evaluated and replaced by the
result treated like a constant.
\begin{verbatim}
variable-name=EVAL(real-expression);
\end{verbatim}
This statement acts like a FORTRAN or C assignment.
The \texttt{real-expression} or \texttt{vector-expression} is
\textbf{evaluated},
and the result is assigned as a constant to the variable or vector on
the left-hand side.
Finally the expression is discarded.
The \texttt{EVAL} function can also be used within an expression, e.~g.:
\begin{verbatim}
vector-name=TABLE(range,EVAL(real-expression));
vector-name={...,EVAL(real-expression),...);
\end{verbatim}
A sequence like the following is permitted:
\begin{verbatim}
...                 // some definitions
X=0;                // create variable X with value
                    // zero
WHILE (X <= 0.10) {
  TWISS,LINE=...;   // uses X=0, 0.01, ..., 0.10
  X=EVAL(X+.01);    // increment variable X by 0.01
                    // CANNOT use: X=X+.01;
}
\end{verbatim}

\subsection{VALUE: Output of Expressions}
\label{sec:value}
\index{Variable!Value}
\index{Value of Variable}
The statement
\begin{verbatim}
VALUE,VALUE=expression-vector;
\end{verbatim}
\index{VALUE}
evaluates a set of expressions using the most recent values of
any operands and prints the results on the standard error file.

\noindent Example:
\begin{verbatim}
REAL A=4;
VALUE,VALUE=TABLE(5,#*A);
REAL P1=5;
REAL P2=7;
VALUE,VALUE={P1,P2,P1*P2-3};
\end{verbatim}
These commands give the results:
\begin{verbatim}
value: {0*A,1*A,2*A,3*A,4*A} = {0,4,8,12,16}
value: {P1,P2,P1*P2-3} = {5,7,32}
\end{verbatim}
This commands serves mainly for printing one or more quantities
which depend on matched attributes.
It also allows use of \textit{OPAL} as a programmable calculator.
One may also tabulate functions.


\section{Miscellaneous Commands}

\subsection{ECHO Statement}
\label{sec:echo}
\index{Echo of Commands}
\index{ECHO}
The \texttt{ECHO} statement has two formats:
\begin{verbatim}
ECHO,MESSAGE=message;
ECHO,message;           // shortcut
\end{verbatim}
\texttt{message} is a string value see~Section~\ref{astring}.
It is immediately transmitted to the \texttt{ECHO} stream.

\subsection{SYSTEM: Execute System Command}
\label{sec:system}
\index{System Command}
\index{SYSTEM}
During an interactive \textit{OPAL} session the command \texttt{SYSTEM}
allows to execute operating system commands.
After execution of the system command, successful or not,
control returns to \textit{OPAL}.
At present this command is only available under UNIX-like OSes (including Linux and macOS).
It has two formats:
\begin{verbatim}
SYSTEM,CMD=string;
SYSTEM,string;         // shortcut
\end{verbatim}
The string see~Section~\ref{astring} \texttt{string} must be a valid operating
system command.

\subsection{SYSTEM Command under UNIX}
Most UNIX commands can be issued directly.

\noindent Example:
\begin{verbatim}
SYSTEM,"ls -l"
\end{verbatim}
causes a listing of the current directory in long form on the terminal.

\section{TITLE Statement}
\label{sec:title}
\index{Page Title}
\index{TITLE}
The \texttt{TITLE} statement has three formats:
\begin{verbatim}
TITLE,STRING=page-header;   // define new page header
TITLE,page-header;          // shortcut for first format
TITLE,STRING="";            // clear page header
\end{verbatim}
\texttt{page-header} is a string value see~Section~\ref{astring}.
It defines the page header which will be used as a title for
subsequent output pages.
Before the first \texttt{TITLE} statement is encountered,
the page header is empty.
It can be redefined at any time.

\section{File Handling}

\subsection{CALL Statement}
\label{sec:call}
\index{CALL}
The \texttt{CALL} command has two formats:
\begin{verbatim}
CALL,FILE=file-name;
CALL,file-name;
\end{verbatim}
\texttt{file-name} is a string see~Section~\ref{astring}.
The statement causes the input to switch to the named file.
Input continues on that file until a \texttt{STOP} or an end of file
is encountered.
\noindent Example:
\begin{verbatim}
CALL,FILE="structure";
CALL,"structure";
\end{verbatim}

\subsection{SAVE Statement}
\label{sec:save}
\index{SAVE}
The \texttt{SAVE} command has two formats:
\begin{verbatim}
SAVE,FILE=file-name
\end{verbatim}
\texttt{file-name} is a string see~Section~\ref{astring}.
The command causes all beam element, beam line, and parameter definitions
to be written on the named file.

\noindent Examples:
\begin{verbatim}
SAVE,FILE="structure";
SAVE,"structure";
\end{verbatim}

\section{IF: Conditional Execution}
\label{sec:if}
\index{IF}
Conditional execution can be requested by an \texttt{IF} statement.
It allows usages similar to the C language \texttt{if} statement:
\begin{verbatim}
IF (logical) statement;
IF (logical) statement; ELSE statement;
IF (logical) { statement-group; }
IF (logical) { statement-group; }
  ELSE { statement-group; }
\end{verbatim}
Note that all statements must be terminated with semicolons (\texttt{;}),
but there is no semicolon after a closing brace.
The statement or group of statements following the \texttt{IF} is
executed if the condition is satisfied.
If the condition is false, and there is an \texttt{ELSE},
the statement or group following the \texttt{ELSE} is executed.

\section{WHILE: Repeated Execution}
\label{sec:while}
\index{WHILE}
Repeated execution can be requested by a \texttt{WHILE} statement.
It allows usages similar to the C language \texttt{while} statement:
\begin{verbatim}
WHILE (logical) statement;
WHILE (logical) { statement-group; }
\end{verbatim}
Note that all statements must be terminated with semicolons (\texttt{;}),
but there is no semicolon after a closing brace.
The condition is re-evaluated in each iteration.
The statement or group of statements following the \texttt{WHILE} is
repeated as long as the condition is satisfied.
Of course some variable(s) must be changed within the \texttt{WHILE} group
to allow the loop to terminate.

\section{MACRO: Macro Statements (Subroutines)}
\label{sec:macro}
\index{MACRO}
Subroutine-like commands can be defined by a \texttt{MACRO} statement.
It allows usages similar to C language function call statements.
A macro is defined by one of the following statements:
\begin{verbatim}
name(formals): MACRO { token-list }
name(): MACRO { token-list }
\end{verbatim}
A macro may have formal arguments, which will be replaced by actual arguments
at execution time. An empty formals list is denoted by \texttt{()}.
Otherwise the \texttt{formals} consist of one or more names,
separated by commas.
The \texttt{token-list} consists of input tokens
(strings, names, numbers, delimiters etc.)
and is stored unchanged in the definition.

A macro is executed by one of the statements:
\begin{verbatim}
name(actuals);
name();
\end{verbatim}
Each actual consists of a set of tokens which replaces all occurrences of
the corresponding formal name.
The actuals are separated by commas.
\noindent Example:
\begin{verbatim}
// macro definitions:
SHOWIT(X): MACRO {
   SHOW, NAME = X;
}
DOIT(): MACRO {
   DYNAMIC,LINE=RING,FILE="DYNAMIC.OUT";
}

// macro calls:
SHOWIT(PI);
DOIT();
\end{verbatim}

\index{Control Statements|)}

\input{footer}