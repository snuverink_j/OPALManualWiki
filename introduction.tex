\chapter{Introduction}\label{chp:Introduction}

\section{Aim of \textit{OPAL} and History}
\textit{OPAL} is a tool for charged-particle optics in
accelerator structures and beam lines.
Using the \texttt{mad} language with extensions, \textit{OPAL} is derived from \texttt{mad9p} and is based
on the \texttt{classic} \ref{bib:classic} class library, which was started in 1995 by an international
collaboration.  IPPL (Independent Parallel Particle Layer) is
the framework which provides parallel particles and fields using data parallel approach.
\textit{OPAL} is built from the ground up as a parallel application exemplifying the fact that HPC (High Performance Computing)
is the third leg of science, complementing theory and the experiment.
HPC is made possible now through the increasingly sophisticated mathematical models and evolving computer power available on the desktop
and in super computer centers. \textit{OPAL} runs on your laptop as well as on the largest HPC clusters available today.

The \textit{OPAL} framework makes it easy to add new features in the form of new
\texttt{C++}~classes.

\textit{OPAL} comes in the following flavors:
\begin{itemize}
\item \textit{OPAL-cycl}
\item \textit{OPAL-t}
\end{itemize}

\textit{OPAL-cycl} tracks particles with 3D space charge including neighboring turns in cyclotrons
with time as the independent variable.

\textit{OPAL-t} is a super-set of \texttt{Impact-t} \ref{qiang2005} and can be used to model guns, injectors, ERLs and complete XFELs excluding the undulator.

It should be noted that not all features of \textit{OPAL} are available in both flavors.\\ The following icon \texttt{DOPAL-t} means that a feature is not yet
available in \textit{OPAL-t}. A similar icon is used for \textit{OPAL-cycl}.

\section{Parallel Processing Capabilities}
\textit{OPAL} is built to harness the power of parallel processing for an improved quantitative understanding
of particle accelerators.
This goal can only be achieved with
detailed 3D modelling capabilities and a sufficient number of simulation particles to obtain meaningful statistics on various
quantities of the particle ensemble such as emittance, slice emittance, halo extension etc.

The following example is exemplifying this fact:



%======================TABLE=============================
\begin{table}[!htb]
\caption[]{Parameters Parallel Performance Example}
\begin{tabular}{lrrrr}
%\toprule
\hline
      Distribution & Particles  & Mesh & Greens Function & Time steps
%      \midrule
\hline
      Gauss 3D & $10^8$ & $1024^3$ & Integrated  & 10 \\
\hline
%\bottomrule
\end{tabular}
\label{tab:pex1}
\end{table}
%=====================TABLE===============================

Figure~\ref{walldrift} shows the parallel efficiency time as a function of used cores for a test example with parameters given in Table~\ref{pex1}.  The data were obtained on a Cray XT5 at the Swiss Center for Scientific Computing.

%======================FIGURE===============================
\begin{figure}[!htb]
\centering
\includegraphics[width=0.75\textwidth]{figures/drift2c1.png}
\caption{Parallel efficiency and particles pushed per {\micros} as a function of cores}
\label{fig:walldrift}
\end{figure}
%===========================================================

\section{Quality Management}
Documentation and quality assurance are given our highest attention since we are convinced that adequate documentation
is a key factor in the usefulness of a code like \textit{OPAL} to study present and future particle accelerators.
 Using tools such as a source code version
control system (\href{https://git-scm.com}{git}), source code documentation using Doxygen (found \href{http://amas.web.psi.ch/docs/opal/html/}{here}) and the extensive user manual you are now enjoying, we are committed to providing users as well as co-developers with
state-of-the-art documentation to \textit{OPAL}.

One example of an non trivial test-example is the PSI DC GUN. In Figure~\ref{guncomp1} the comparison between \texttt{Impact-t} and \textit{OPAL-t} is shown. This example is part of the regression test suite
that is run every night. The input file is found in Section~\ref{examplesbeamlines}.

Misprints and obscurity are almost inevitable in a document of this size.
Comments and {\em active contributions}  from readers are therefore most welcome.
They may be sent to \texttt{andreas.adelmann@psi.ch}.


%======================FIGURE===============================
\begin{figure}[!htb]
\centering
   \includegraphics[width=0.45\textwidth]{figures/Gun/GunCompEn.png}
   \hspace{0.05\textwidth}
   \includegraphics[width=0.45\textwidth]{figures/Gun/GunCompEx.png}
   \caption{Comparison of energy and emittance in $x$ between \texttt{Impact-t} and \textit{OPAL-t}}
   \label{fig:guncomp1}
\end{figure}
%===========================================================



\section{Output}
The phase space is stored in the H5hut file-format \ref{bib:howison2010} and can be analyzed
using e.g. H5root \ref{bib:schietinger}. The frequency
of the data output (phase space and some statistical quantities) can be controlled using the \texttt{OPTION} statement see~Section~\ref{option},
 with the flag \texttt{PSDUMPFREQ}. The file is named like in input file but with the extension {\tt .h5}.

A SDDS compatible ASCII file with statistical beam parameters is written to a file with extension {\tt .stat}. The frequency with which this data is written can be controlled with the \texttt{OPTION} statement see~Section~\ref{option} with the flag \texttt{STATDUMPFREQ}.
%======================FIGURE===============================
\begin{figure}[!htb]
\centering
 \includegraphics[width=0.32\textwidth]{figures/H5rootPicture1.png}
 \includegraphics[width=0.32\textwidth]{figures/H5rootPicture2.png}
 \includegraphics[width=0.31\textwidth]{figures/H5rootPicture3.png}
\caption[]{H5root enables a variety of data analysis and post processing task on \textit{OPAL} data}
%  \caption{Example of a longitudinal phase space shown with H5root}
 \label{fig:h5root1}
\end{figure}
%===========================================================

\section{Change History}
See Appendix~\ref{changelog} for a detailed list of changes in \textit{OPAL}.
\section{Known Issues and Limitations}
\subsection{\textit{OPAL-cycl}}
\begin{itemize}
    \item Restart with the option \texttt{PSDUMPLOCALFRAME} does not work yet,
    \item In complicated geometries such as spiral inflectors, proper particle deletion at the boundary sometimes fails.
\end{itemize}
\section{Acknowledgments}
The contributions of various individuals and groups are acknowledged in the relevant chapters, however a few individuals have or had considerable influence on the
development of \textit{OPAL}, namely Chris Iselin, John Jowett, Julian Cummings, Ji Qiang, Robert Ryne and Stefan Adam. For the \texttt{H5root}~ visualization tool credits go to Thomas Schietinger.

The following
individuals are acknowledged for past contributions: Yuanjie Bi, Jianjun Yang, Colwyn Gulliford, Hao Zha, Christopher Mayes and Tulin Kaman.

%- - - - - - - - - - - - - - - SECTION- - - - - - - - - - - - - - - - - - -
\section{Citation}
Please cite \textit{OPAL} in the following way:
\begin{verbatim}
@techreport{opal:1,
title = {The OPAL (Object Oriented Parallel Accelerator Library) Framework},
author = {Andreas Adelmann, Christian Baumgarten, Matthias Frey, Achim Gsell, Valeria Rizzoglio, 
Jochem Snuverink (PSI) and Christof Metzger-Kraus and Yves Ineichen and 
Steve Russell (LANL) and Chuan Wang (CIAE) and 
Suzanne Sheehy and Chris Rogers (RAL) and
Daniel Winklehner (MIT)},
institution = {Paul Scherrer Institut},
number = {PSI-PR-08-02},
year = {(2008-2017)}
}
\end{verbatim}
