\input{header}

\chapter{Tutorial}
\label{chp:tutorial}
This chapter will provide a jump start describing some of the most common used features of \textit{OPAL}. The complete set of examples can be found
and downloaded at \url{https://gitlab.psi.ch/OPAL/src/wikis/home}. All examples are requiring a small amount of computing resources and run on a single core, but can be used
efficiently on up to 8 cores. \textit{OPAL} scales in the weak sense, hence for a higher concurrency one has to increase the problem size i.e. number of
macro particles and the grid size, which is beyond this tutorial.

\section{Starting OPAL}
The name of the application is \texttt{opal}. When called without any argument an interactive session is started.
\begin{verbatim}
\$ opal
Ippl> CommMPI: Parent process waiting for children ...
Ippl> CommMPI: Initialization complete.
>                ____  _____       ___
>               / __ \|  __ \ /\   | |
>              | |  | | |__) /  \  | |
>              | |  | |  ___/ /\ \ | |
>              | |__| | |  / ____ \| |____
>               \____/|_| /_/    \_\______|
OPAL >
OPAL > This is OPAL (Object Oriented Parallel Accelerator Library) Version 2.0.0 ...
OPAL >
OPAL > Please send cookies, goodies or other motivations (wine and beer ... )
OPAL > to the OPAL developers opal@lists.psi.ch
OPAL >
OPAL > Time: 16.43.23 date: 30/05/2017
OPAL > Reading startup file ``/Users/adelmann/init.opal''.
OPAL > Finished reading startup file.
==>
\end{verbatim}
One can exit from this session with the command \texttt{QUIT;} (including the semicolon).

For batch runs \textit{OPAL} accepts the following command line arguments:\\
\begin{tabular}{|l|l|p{10cm}|}
\hline
\tabhead Argument & Values & Function \\
\hline
-{}-input & \textless file \textgreater & the input file. Using ``-{}-input'' is optional. Instead the input file can be provided either as first or as last argument.\\
-{}-info & {0} -- {5} & {controls the amount of output to the command line. {0} means no or scarce output, {5} means a lot of output. Default: {1}.\\ Full support currently only in \textit{OPAL-t}.} \\
-{}-restart & {-1} -- \textless Integer\textgreater & restarts from given step in file with saved phase space. Per default \textit{OPAL} tries to restart from a file \textless file\textgreater.h5 where \textless file\textgreater is the input file without extension. {-1} stands for the last step in the file. If no other file is specified to restart from and if the last step of the file is chosen, then the new data is appended to the file. Otherwise the data from this particular step is copied to a new file and all new data appended to the new file.\\
-{}-restartfn & \textless file\textgreater & a file in H5hut format from which \textit{OPAL} should restart.\\
-{}-help & & Displays a summary of all command-line arguments and then quits.\\
-{}-version & & Prints the version and then quits.\\
\hline
\end{tabular}

\vspace{3pt}
Example:
\begin{verbatim}
opal input.in --restartfn input.h5 --restart -1 --info 3
\end{verbatim}


\section{Auto-phase Example}
\label{sec:trackautoph}
This is a partial complete example. First we have to set \textit{OPAL} in \texttt{AUTOPHASE} mode, as described in Section~\ref{option} and for example set the nominal phase to
$-3.5^{\circ}$). The way how \textit{OPAL} is computing the phases is explained in Appendix~\ref{autophasing}.
\begin{verbatim}
Option, AUTOPHASE=4;

REAL FINSS_RGUN_phi= (-3.5/180*Pi);
\end{verbatim}

The cavity would be defined like
\begin{verbatim}
FINSS_RGUN: RFCavity, L = 0.17493, VOLT = 100.0,
	FMAPFN = "FINSS-RGUN.dat",
	ELEMEDGE =0.0, TYPE = "STANDING", FREQ = 2998.0,
	LAG = FINSS_RGUN_phi;
\end{verbatim}
with \texttt{FINSS\_RGUN\_phi} defining the off crest phase. Now a normal \texttt{TRACK} command can be executed. A file containing the values of maximum phases is created, and has the format like:
\begin{verbatim}
1
FINSS_RGUN
2.22793
\end{verbatim}
with the first entry defining the number of cavities in the simulation.


\section{Examples of Particle Accelerators and Beamlines}
\label{sec:examplesbeamlines}

\examplefromfile{examples/OBLA-Gun/OBLA-Gun.in}


%Figure~\ref{guncomp2} shows the excellent agreement between \texttt{Impact-t} and \textit{OPAL-t}.
%
%\begin{figure}[ht]
% \begin{center}
%   \includegraphics[width=0.6\linewidth,angle=90]{figures/Gun/GunCompEn}
%   \includegraphics[width=0.6\linewidth,angle=90]{figures/Gun/GunCompEx}
%   \caption{Comparison of energy and emittance in $x$ between \texttt{Impact-t} and \textit{OPAL-t}}
%   \label{fig:guncomp2}
% \end{center}
%\end{figure}

\subsection{PSI XFEL 250 MeV Injector}
\label{sec:felinj}
\index{XFEL}
\index{INJECTOR}
\examplefromfile{examples/diagnostic.in}



\subsection{PSI Injector II Cyclotron}
\label{sec:inj2}
\index{INJECTOR II}
\index{CYCLOTRON}
%%%%%%%%%%
Injector II is a separated sector cyclotron specially designed for pre-acceleration (inject: {870}{keV}, extract: {72}{MeV} )
of high intensity proton beam for Ring cyclotron. It has 4 sector magnets, two double-gap acceleration cavities
(represented by 2 single-gap cavities here) and two single-gap flat-top cavities.

Following is an input file of {\bfseries Single Particle Tracking mode} for PSI Injector II cyclotron.

\examplefromfile{examples/opal-cycl.in}

To run opal on a single node, just use this command:
\begin{verbatim}
 opal testinj2-1.in
\end{verbatim}

Here shows some pictures using the resulting data from single particle tracking using \textit{OPAL-cycl}.

Left plot of Figure~\ref{Inj2 reference orbit and tune} shows the accelerating orbit of reference particle. After 106 turns, the energy increases from {870}{keV} at the injection point to {72.16}{MeV} at the deflection point.

%======================FIGURE===============================
\begin{figure}[tb]
\centering
   \includegraphics[width=0.45\textwidth]{figures/cyclotron/AEO_Injector2.png}
   \includegraphics[width=0.40\textwidth,angle=90]{figures/cyclotron/nurnuz_Inj2.png}
   \caption{Reference orbit(left) and tune diagram(right) in Injector II  }
   \label{fig:Inj2 reference orbit and tune}
\end{figure}
%===========================================================


From theoretic view, there should be an eigen ellipse for any given energy in stable area of a fixed accelerator structure. Only when the initial phase space
shape matches its eigen ellipse, the oscillation of beam envelop amplitude will get minimal and the transmission efficiency get maximal.
We can calculate the eigen ellipse by single particle tracking using betatron oscillation property of off-centered particle as following: track
an off-centered particle and record its coordinates and momenta at the same azimuthal position for each revolution.
Figure~\ref{eigen} shows the eigen ellipse at symmetric line of sector magnet for energy of {2}{MeV} in Injector II.

%======================FIGURE===============================
\begin{figure}[tb]
\centering
   \includegraphics[width=0.45\textwidth]{figures/cyclotron/RadialEigen_Inj2.png}
   \includegraphics[width=0.45\textwidth]{figures/cyclotron/VertEigen_Inj2.png}
   \caption{Radial and vertical eigenellipse at {2}{MeV} of Injector II}
   \label{fig:eigen}
\end{figure}
%===========================================================


Right plot of Figure~\ref{Inj2 reference orbit and tune} shows very good agreement of the tune diagram by \textit{OPAL-cycl} and FIXPO.
The trivial discrepancy should come from the methods they used.
In FIXPO, the tune values are obtained according to the crossing points of the initially displaced particle. Meanwhile, in \textit{OPAL-cycl}, the Fourier
analysis method is used to manipulate orbit difference between the reference particle and an initially displaced particle.
The frequency point with the biggest amplitude is the betatron tune value at the given energy.

%%%%%%%%%%%%%%
Following is the input file for single bunch tracking with space charge effects in Injector II.
%%%%%%%%%%%%%%

\examplefromfile{examples/opal-cycl-sc.in}

To run opal on single node, just use this command:
\begin{verbatim}
 # opal testinj2-2.in
\end{verbatim}

To run opal on N nodes in parallel environment interactively, use this command instead:
\begin{verbatim}
 # mpirun -np N opal testinj2-2.in
\end{verbatim}

If restart a job from the last step of an existing \textit{.h5} file, add a new argument like this:
\begin{verbatim}
 # mpirun -np N opal testinj2-2.in --restart -1
\end{verbatim}
Figure~\ref{cyclParameters,cyclphasespace} are simulation results, shown by  H5ROOT code.

%======================FIGURE===============================
\begin{figure}[tb]
\centering
    \includegraphics[width=0.45\textwidth]{figures/cyclotron/Inj2-ENERGY-TIME.png}
    \includegraphics[width=0.45\textwidth]{figures/cyclotron/Inj2-B-ref-z-TrackStep.png}
    \caption{Energy Vs. time (left) and external B field Vs. track step (Right, only show for about 2 turns)}
    \label{fig:cyclParameters}
\end{figure}
%===========================================================


%======================FIGURE===============================
\begin{figure}[tb]
\centering
    \includegraphics[width=0.3\textwidth]{figures/cyclotron/Inj2-z-pz-step-870KeV.png}
    \includegraphics[width=0.3\textwidth]{figures/cyclotron/Inj2-z-pz-step-15MeV.png}
    \includegraphics[width=0.3\textwidth]{figures/cyclotron/Inj2-z-pz-step-30MeV.png}
    \caption{Vertical phase at different energy from left to right: {0.87}{MeV}, {15}{MeV} and {35}{MeV}}
    \label{fig:cyclphasespace}
\end{figure}
%===========================================================



%%%%%%%%%%%%%%
\subsection{PSI Ring Cyclotron}
\label{sec:Ring}
\index{RING}
\index{CYCLOTRON}
From the view of numerical simulation, the difference between Injector II and Ring cyclotron comes from two aspects:
\begin{description}
\item[B Field] The structure of Ring is totally symmetric, the field on median plain is periodic
along azimuthal direction, \textit{OPAL-cycl} take this advantage to only store \frac{1}{8} field data to save memory.

\item[RF Cavity] In the Ring, all the cavities are typically single gap with some parallel displacement from its
radial position.\textit{OPAL-cycl} have an argument \texttt{PDIS} to manipulate this issue.
\end{description}

\begin{figure}[ht]
\centering
    \includegraphics[width=6cm]{figures/cyclotron/AEO_Ring.png}
    \includegraphics[width=6cm]{figures/cyclotron/nurnuz_Ring.png}
    \caption{Reference orbit(left) and tune diagram(right) in Ring cyclotron }
    \label{fig:Ring reference orbit and tune}
\end{figure}

Figure~\ref{Ring reference orbit and tune} shows a single particle tracking result and tune calculation result in the PSI Ring cyclotron.
Limited by size of the user guide, we don't plan to show too much details as in Injector II.

\clearpage


\section{Translate Old to New Distribution Commands}
\label{sec:oldtonewdist}
\index{Distribution!Translate Old}
As of \textit{OPAL} 1.2, the distribution command see~Chapter~\ref{distribution} was changed significantly. Many of the changes were internal to the code, allowing us to more easily add new distribution command options. However, other changes were made to make creating a distribution easier, clearer and so that the command attributes were more consistent across distribution types. Therefore, we encourage our users to refer to Chapter \ref{distribution} when creating any new input files, or if they wish to update existing input files.

With the new distribution command, we did attempt as much as possible to make it backward compatible so that existing \textit{OPAL} input files would still work the same as before, or with small modifications. In this section of the manual, we will give several examples of distribution commands that will still work as before, even though they have antiquated command attributes. We will also provide examples of commonly used distribution commands that need small modifications to work as they did before.

\textbf{\emph{An important point to note is that it is very likely you will see small changes in your simulation even when the new distribution command is nominally generating particles in exactly the same way.}} This is because random number generators and their seeds will likely not be the same as before. These changes are only due to \textit{OPAL} using a different sequence of numbers to create your distribution, and not because of errors in the calculation. (Or at least we hope not.)

\subsection{\texttt{GUNGAUSSFLATTOPTH} and \texttt{ASTRAFLATTOPTH} Distribution Types}
\label{sec:oldtonewdistgungaussandastra}
\index{Distribution!Translate GUNGAUSSFLATTOPTH}
\index{Distribution!Translate ASTRAFLATTOPTH}
The \texttt{GUNGAUSSFLATTOPTH} and \texttt{ASTRAFLATTOPTH} distribution types see~Section~\ref{gungaussflattopthdisttype,astraflattopthdisttype} are two common types previously implemented to simulate electron beams emitted from photocathodes in an electron photoinjector. These are no longer explicitly supported and are instead now defined as specialized sub-types of the distribution type \texttt{FLATTOP}. That is, the \emph{emitted} distributions represented by \texttt{GUNGAUSSFLATTOPTH} and \texttt{ASTRAFLATTOPTH} can now be easily reproduced by using the \texttt{FLATTOP} distribution type and we would encourage use of the new command structure.

Having said this, however, old input files that use the \texttt{GUNGAUSSFLATTOPTH} and \texttt{ASTRAFLATTOPTH} distribution types will still work as before, with the following exception. Previously, \textit{OPAL} had a Boolean \texttt{OPTION} command \texttt{FINEEMISSION} (default value was \texttt{TRUE}). This \texttt{OPTION} is no longer supported. Instead you will need to set the distribution attribute \texttt{EMISSIONSTEPS} see~Table~\ref{distattremitted} to a value that is 10 $\times$ the value of the distribution attribute \texttt{NBIN} see~Table~\ref{distattruniversal} in order for your simulation to behave the same as before.
\input{footer}

\subsection{\texttt{FROMFILE}, \texttt{GAUSS} and \texttt{BINOMIAL} Distribution Types}
\index{Distribution!Translate FROMFILE}
\index{Distribution!Translate GAUSS}
\index{Distribution!Translate BINOMIAL}
The \texttt{FROMFILE} see~Section~\ref{fromfiledisttype}, \texttt{GAUSS} see~Section~\ref{gaussdisttype} and \texttt{BINOMIAL} see~Section~\ref{binomialdisttype} distribution types have changed from previous versions of \textit{OPAL}. However, legacy distribution commands should work as before with one exception. If you are using \textit{OPAL-cycl} then your old input files will work just fine. However, if you are using \textit{OPAL-t} then you will need to set the distribution attribute \texttt{INPUTMOUNITS} see~Section~\ref{unitsdistattributes} to:
\begin{verbatim}
INPUTMOUNITS = EV
\end{verbatim}