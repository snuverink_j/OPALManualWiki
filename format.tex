\chapter{Command Format}
\label{chp:format}
\index{Format!Commands}
\index{Command!Format|(}
All flavors of \textit{OPAL} using the same input language the \texttt{MAD} language. The language dialect here is
ajar to \textit{MAD9}, for hard core \textit{MAD}eight users there is a conversion guide.

It is the first time that
machines such as cyclotrons, proton and electron linacs can be described within the same language
in the same simulation framework.
\section{Statements and Comments}
\label{sec:statements}
\index{Statement}
\index{Comment}
Input for \textit{OPAL} is free format, and the line length is not limited.
During reading, input lines are normally printed on the echo file,
but this feature can be turned off for long input files.
The input is broken up into tokens (words, numbers, delimiters etc.),
which form a sequence of commands, also known as statements.
Each statement must be terminated by a semicolon (\texttt{;}),
and long statements can be continued on any number of input lines.
White space, like blank lines, spaces, tabs,
and newlines are ignored between tokens.
Comments can be introduced with two slashes (\texttt{//})
and any characters following the slashes on the same line are ignored.

The C~convention for comments (\texttt{/* ... */}) is also accepted.
The comment delimiters \texttt{/*} and \texttt{*/} can be nested;
this allows to ``comment out'' sections of input.

In the following descriptions,
words in \texttt{lower case} stand for syntactic units
which are to be replaced by actual text.
\texttt{UPPER CASE} is used for keywords or names.
These must be entered as shown.
Ellipses (\texttt{...}) are used to indicate repetition.

The general format for a command is
\begin{verbatim}
keyword,attribute,...,attribute;
label:keyword,attribute,...,attribute;
\end{verbatim}
It has three parts:
\begin{enumerate}
\item The \texttt{label} is required for a definition statement.
  \index{Command!Label}
  \index{Label!Command}
  Its must be an identifier see~Section~\ref{label} and gives a name to the
  stored command.
\item The \texttt{keyword} identifies the action desired.
  \index{Command!Keyword}
  \index{Keyword!Command}
  It must be an identifier see~Section~\ref{label}.
\item Each \texttt{attribute} is entered in one of the forms
\begin{verbatim}
attribute-name
attribute-name=attribute-value
attribute-name:=attribute-value
\end{verbatim}
and serves to define data for the command, where:
\begin{itemize}
\item The \texttt{attribute-name} selects the attribute,
  \index{Attribute!Name}
  \index{Name!Attribute}
  it must be an identifier see~Section~\ref{label}.
\item The \texttt{attribute-value} gives it a value see~Section~\ref{attribute}.
  \index{Attribute!Value}
  \index{Value!Attribute}
  When the attribute value is a constant or an expression preceded by
  the delimiter \texttt{=} it is evaluated immediately and the result
  is assigned to the attribute as a constant.
  When the attribute value is an expression preceded by the delimiter
  \texttt{:=} the expression is retained and re-evaluated whenever one
  of its operands changes.
\end{itemize}
Each attribute has a fixed attribute type see~Section~\ref{attribute}.\\
The \texttt{attribute-value} can only be left out for logical
attributes, this implies a \texttt{true} value.
\end{enumerate}

When a command has a \texttt{label},
\textit{OPAL} keeps the command in memory.
This allows repeated execution of the same command
by entering its label only:
\begin{verbatim}
label;
\end{verbatim}
or to re-execute the command with modified attributes:
\begin{verbatim}
label,attribute,...,attribute;
\end{verbatim}
If the label of such a command appears together with new attributes,
\textit{OPAL} makes a copy of the stored command, replaces the attributes entered,
and then executes the copy:
\begin{verbatim}
QF:QUADRUPOLE,L=1,K1=0.01; // first definition of QF
QF,L=2;                    // redefinition of QF

MATCH;
...
LMD:LMDIF,CALLS=10;           // first execution of LMD
LMD;                          // re-execute LMD with
                              // the same attributes
LMD,CALLS=100,TOLERANCE=1E-5; // re-execute LMD with
                              // new attributes
ENDMATCH;
\end{verbatim}

\section{Identifiers or Labels}
\label{sec:label}
\index{Label}
\index{Name}
\index{Identifier}
\index{Keyword}
An identifier refers to a keyword, an element, a beam line, a variable,
an array, etc.

A label begins with a letter, followed by an arbitrary number of letters,
digits, periods (\texttt{.}), underscores (\texttt{\_}).
Other special characters can be used in a label,
but the label must then be enclosed in single or double quotes.
It makes no difference which type of quotes is used,
as long as the same are used at either end.
The preferred form is double quotes.
The use of non-numeric characters is however strongly discouraged,
since it makes it difficult to subsequently process a \textit{OPAL} output with
another program.

When a name is not quoted, it is converted to upper case;
the resulting name must be unique.
An identifier can also be generated from a
string expression see~Section~\ref{astring}.

\section{Command Attribute Types}
\label{sec:attribute}
\index{Attribute!Value}
\index{Value!Attribute}
An object attribute is referred to by the syntax
\begin{verbatim}
object-name->attribute-name
\end{verbatim}
If the attribute is an array see~Section~\ref{anarray},
one of its components is found by the syntax
\begin{verbatim}
object-name->attribute-name[index]
\end{verbatim}
The following types of command attributes are available in \textit{OPAL}:
\begin{itemize}
\item String see~Section~\ref{astring},
\item Logical see~Section~\ref{alogical},
\item Real expression see~Section~\ref{areal},
\item Deferred expression see~Section~\ref{adefer},
\item Place see~Section~\ref{aplace},
\item Range see~Section~\ref{arange},
\item Constraint see~Section~\ref{aconstraint},
\item Variable Reference see~Section~\ref{areference}
\item Regular expression see~Section~\ref{wildcard}
\item Token list see~Section~\ref{toklist}.
\item Array see~Section~\ref{anarray} of
  \begin{itemize}
  \item Logical see~Section~\ref{logarray},
  \item Real see~Section~\ref{realarray},
  \item String see~Section~\ref{strarray},
  \item Token lists see~Section~\ref{tokarray},
  \end{itemize}
\end{itemize}
See also:
\begin{itemize}
\item Operators see~Table~\ref{operator},
\item Functions see~Table~\ref{realfun},
\item Array functions see~Table~\ref{arrayfun},
\item Real functions of arrays see~Table~\ref{compfun},
\item Operand see~Section~\ref{operand},
\item Random generators see~Section~\ref{adefer}.
\end{itemize}

\section{String Attributes}
\label{sec:astring}
\index{String}
A string attribute makes alphanumeric information available,
e.g. a title, file name, element class name, or an option.
It can contain any characters, enclosed in single (\texttt{'})
or double (\texttt{"}) quotes.
However, if it contains a quote, this character must be doubled.
Strings can be concatenated using the \texttt{\&} operator see~Table~\ref{stroperator}.
An operand in a string can also use the
function \texttt{STRING} see~Table~\ref{stringfun}.
String values can occur in string arrays see~Section~\ref{anarray}.

\begin{table}[!htb]
   \begin{center}
    \caption{String Operator in \textit{OPAL}}
    \label{tab:stroperator}
    \begin{tabular}{|l|p{0.5\textwidth}|l|l|}
      \hline
      \tabhead Operator & Meaning & result type & operand types \\
      \hline
      \texttt{X \& Y} & concatenate the strings \texttt{X} and \texttt{Y}.
      String concatenations are always evaluated immediately when read. &
      string &string,string \\
      \hline
    \end{tabular}
  \end{center}
\end{table}

\begin{table}[!htb] \footnotesize
  \begin{center}
    \caption{String Function in \textit{OPAL}}
    \label{tab:stringfun}
    \begin{tabular}{|l|p{0.5\textwidth}|l|l|}
      \hline
      \tabhead Function & Meaning & result type & argument type \\
      \hline
      \texttt{STRING(X)} &
      return string representation of the value
      of the numeric expression \texttt{X} &
      string &real \\
      \hline
    \end{tabular}
  \end{center}
\end{table}
\par
\noindent Examples:
\begin{verbatim}
TITLE,"This is a title for the program run ""test""";
CALL,FILE="save";

REAL X=1;
TWISS,LINE=LEP&STRING(X+1);
\end{verbatim}
The second example converts the value of the expression ``X+1'' to a
string and appends it to ``LEP'', giving the string ``LEP2''.

\section{Logical Expressions}
\label{sec:alogical}
\index{Logical}
Many commands in \textit{OPAL} require the setting of logical values (flags)
to represent the on/off state of an option.
A logical value is represented by one of the values \texttt{TRUE}
or \texttt{FALSE}, or by a logical expression.
A logical expression can occur in logical arrays see~Section~\ref{logarray}.
\par
A logical expression has the same format and operator precedence as a
logical expression in C.
It is built from logical operators see~Table~\ref{logoperator} and logical
operands:
\begin{verbatim}
relation      ::= "TRUE" |
                  "FALSE" |
                  real-expr rel-operator real-expr

rel-operator  ::= "==" | "!=" | "<" | ">" | ">=" | "<="

and-expr      ::= relation | and-expr "&&" relation

logical-expr  ::= and-expr | logical-expr "||" and-expr
\end{verbatim}

\begin{table}[!htb] \footnotesize
  \begin{center}
    \caption{Logical Operators in \textit{OPAL}}
    \label{tab:logoperator}
    \begin{tabular}{|l|p{0.5\textwidth}|l|l|}
      \hline
      \tabhead Operator & Meaning & result type & operand type \\
      \hline
      \texttt{X $<$ Y} & true, if \texttt{X} is less than \texttt{Y} &
      logical &real,real \\
      \texttt{X $<=$ Y} & true, if \texttt{X} is not greater than \texttt{Y} &
      logical &real,real \\
      \texttt{X $>$ Y} & true, if \texttt{X} is greater than \texttt{Y} &
      logical &real,real \\
      \texttt{X $>=$ Y} & true, if \texttt{X} is not less than \texttt{Y} &
      logical &real,real \\
      \texttt{X $==$ Y} & true, if \texttt{X} is equal to \texttt{Y} &
      logical &real,real \\
      \texttt{X $!=$ Y} & true, if \texttt{X} is not equal to \texttt{Y} &
      logical &real,real \\
      \texttt{X \&\& Y} & true, if both \texttt{X} and \texttt{Y} are true &
      logical &logical,logical \\
      \texttt{X} $\textbar$ \texttt{Y} &
      true, if at least one of \texttt{X} and \texttt{Y} is true &
      logical &logical,logical \\
      \hline
    \end{tabular}
  \end{center}
\end{table}
\par
\noindent Example:
\begin{verbatim}
OPTION,ECHO=TRUE; // output echo is desired
\end{verbatim}
When a logical attribute is not entered,
its default value is always \texttt{FALSE}.
When only its name is entered, the value is set to \texttt{TRUE}:
\begin{verbatim}
OPTION,ECHO;      // same as above
\end{verbatim}
\noindent Example of a logical expression:
\begin{verbatim}
X>10 && Y<20 || Z==15
\end{verbatim}

\section{Real Expressions}
\label{sec:areal}
\index{Real}
To facilitate the definition of interdependent quantities,
any real value can be entered as an arithmetic expression.
When a value used in an expression is redefined by the user
or changed in a matching process,
the expression is re-evaluated.
Expression definitions may be entered in any order.
\textit{OPAL} evaluates them in the correct order before it performs
any computation.
At evaluation time all operands used must have values assigned.
A real expression can occur in real arrays see~Section~\ref{realarray}.

A real expression is built from operators see~Table~\ref{operator} and
operands see~Section~\ref{operand}:

\begin{footnotesize}
\begin{verbatim}
real-ref  ::= real-variable |
              real-array "[" index "]" |
              object "->" real-attribute |
              object "->" real-array-attribute "[" index "]" |

table-ref ::= table "@" place "->" column-name

primary   ::= literal-constant |
              symbolic-constant |
              "#" |
              real-ref |
              table-ref |
              function-name "(" arguments ")" |
              (real-expression)

factor    ::= primary |
              factor "^" primary

term      ::= factor |
              term "*" factor |
              term "/" factor

real-expr ::= term |
              "+" term |
              "-" term |
              real-expr "+" term |
              real-expr "-" term |
\end{verbatim}
\end{footnotesize}

It may contain functions see~Table~\ref{realfun},
Parentheses indicate operator precedence if required.
Constant sub-expressions are evaluated immediately,
and the result is stored as a constant.

\section{Operators}
\index{Expression!Operator}
\index{Operator}
An expression can be formed using operators see~Table~\ref{operator} and
functions see~Table~\ref{realfun}
acting on operands see~Section~\ref{operand}.

\begin{table}[!htb]  \footnotesize
  \begin{center}
    \caption{Real Operators in \textit{OPAL}}
    \label{tab:operator}
    \begin{tabular}{|l|l|l|l|}
      \hline
      \tabhead Operator & Meaning & result type & operand type(s) \\
      \hline
      \textbf{Real operators with one operand}\\
      \hline
      \texttt{+ X} & unary plus, returns \texttt{X} &
      real &real \\
      \texttt{- X} & unary minus, returns the negative of \texttt{X} &
      real &real \\
      \hline
      \textbf{Real operators with two operands} \\
      \hline
      \texttt{X + Y} & add \texttt{X} to \texttt{Y} &
      real & real,real \\
      \texttt{X - Y} & subtract \texttt{Y} from \texttt{X} &
      real & real,real \\
      \texttt{X * Y} & multiply \texttt{X} by \texttt{Y} &
      real &real,real \\
      \texttt{X / Y} & divide \texttt{X} by \texttt{Y} &
      real &real,real \\
      \texttt{X \^\ Y} &
      power, return \texttt{X} raised to the power \texttt{Y}
      ($\mathtt{Y} > 0$) & real &real,real \\
      \hline
    \end{tabular}
  \end{center}
\end{table}

\begin{table}[!htb] \footnotesize
  \begin{center}
    \caption{Real Functions in \textit{OPAL}}
    \label{tab:realfun}
    \begin{tabular}{|l|l|l|l|}
      \hline
      \tabhead Function & Meaning & result type & argument type(s) \\
      \hline
      \textbf{Real functions with no arguments} \\
      \hline%
      \texttt{RANF()} & random number, uniform distribution in [0,1) &
      real &- \\
      \texttt{GAUSS()} & random number, Gaussian distribution with $\mu=0$ and $\sigma=1$ &
      real &- \\
      \texttt{GETEKIN()} & returns the kinetic energy of the bunch (MeV) &
      real &- \\
      \texttt{USER0()} & random number, user-defined distribution &
      real &- \\
      \hline
      \textbf{Real functions with one argument} \\
      \hline
      \texttt{TRUNC(X)} &
      truncate \texttt{X} towards zero (discard fractional part) &
      real &real \\
      \texttt{ROUND(X)} & round \texttt{X} to nearest integer &
      real &real \\
      \texttt{FLOOR(X)} & return largest integer not greater than \texttt{X} &
      real &real \\
      \texttt{CEIL(X)} & return smallest integer not less than \texttt{X} &
      real &real \\
      \texttt{SIGN(X)} & return sign of \texttt{X}
      (+1 for \texttt{X} positive, -1 for \texttt{X} negative,
      0 for \texttt{X} zero) & real &real \\
      \texttt{SQRT(X)} & return square root of \texttt{X} &
      real &real \\
      \texttt{LOG(X)} & return natural logarithm of \texttt{X} &
      real &real \\
      \texttt{EXP(X)} & return exponential to the base $e$ of \texttt{X} &
      real &real \\
      \texttt{SIN(X)} & return trigonometric sine of \texttt{X} &
      real &real \\
      \texttt{COS(X)} & return trigonometric cosine of \texttt{X} &
      real &real \\
      \texttt{ABS(X)} & return absolute value of \texttt{X} &
      real &real \\
      \texttt{TAN(X)} & return trigonometric tangent of \texttt{X} &
      real &real \\
      \texttt{ASIN(X)} & return inverse trigonometric sine of \texttt{X} &
      real &real \\
      \texttt{ACOS(X)} & return inverse trigonometric cosine of \texttt{X} &
      real &real \\
      \texttt{ATAN(X)} & return inverse trigonometric tangent of \texttt{X} &
      real &real \\
      \texttt{TGAUSS(X)} &
      random number, Gaussian distribution with $\sigma$=1,
      truncated at \texttt{X} &
      real &real \\
      \texttt{USER1(X)} &
      random number, user-defined distribution with one parameter &
      real &real \\
      \texttt{EVAL(X)} &
      evaluate the argument immediately and transmit it as a constant &
      real &real \\
      \hline
      \textbf{Real functions with two arguments} \\
      \hline
      \texttt{ATAN2(X,Y)} &
      return inverse trigonometric tangent of \texttt{Y/X} &
      real &real,real \\
      \texttt{MAX(X,Y)} & return the larger of \texttt{X}, \texttt{Y} &
      real &real,real \\
      \texttt{MIN(X,Y)} &
      return the smaller of \texttt{X}, \texttt{Y} &
      real &real,real \\
      \texttt{MOD(X,Y)} &
      return the largest value less than \texttt{Y}
      which differs from \texttt{X} by a multiple of \texttt{Y} &
      real &real,real \\
      \texttt{USER2(X,Y)} &
      random number, user-defined distribution with two parameters &
      real &real,real \\
      \hline
    \end{tabular}
  \end{center}
\end{table}

\begin{table}[!htb] \footnotesize
  \begin{center}
    \caption{Real Functions of Arrays in \textit{OPAL}}
    \label{tab:arrayfun}
    \begin{tabular}{|l|p{0.45\textwidth}|l|l|}
      \hline
      \tabhead Function & Meaning & result type & operand type \\
      \hline
      \texttt{VMAX(X,Y)} &
      return largest array component &real&real array\\
      \texttt{VMIN(X,Y)} &
      return smallest array component &real&real array\\
      \texttt{VRMS(X,Y)} &
      return rms value of an array &real&real array\\
      \texttt{VABSMAX(X,Y)} &
      return absolute largest array component &real&real array\\
      \hline
    \end{tabular}
  \end{center}
\end{table}

Care must be used when an ordinary expression contains a random generator.
It may be re-evaluated at unpredictable times, generating a new value.
However, the use of a random generator in an assignment expression is safe.
\noindent Examples:
\begin{verbatim}
D:DRIFT,L=0.01*RANF();    // a drift space with rand. length,
                          // may change during execution.
REAL P=EVAL(0.001*TGAUSS(X));  // Evaluated once and stored as a constant.
\end{verbatim}

\section{Operands in Expressions}
\label{sec:operand}
\index{Expression!Operand}
\index{Operand}
A real expression may contain the operands listed in the following
subsections.

\subsection{Literal Constants}
Numerical values are entered like FORTRAN constants.
Real values are accepted in INTEGER or REAL format.
The use of a decimal exponent, marked by the letter \texttt{D} or \texttt{E},
is permitted.

\noindent Examples:
\begin{verbatim}
1, 10.35, 5E3, 314.1592E-2
\end{verbatim}

\subsection{Symbolic constants}
\index{Expression!Constant}
\index{Constant}
\textit{OPAL} recognizes a few built-in
mathematical and physical constants see~Table~\ref{constant}.
Their names must not be used for user-defined labels.
Additional symbolic constants may be defined see~Section~\ref{constant} to
simplify their repeated use in statements and expressions.

\begin{table}[!htb] \footnotesize
  \begin{center}
    \caption{Predefined Symbolic Constants}
    \label{tab:constant}
    \begin{tabular}{|l|c|c|c|}
      \hline
      \tabhead \textit{OPAL} name & Mathematical symbol & Value & Unit \\
      \hline
      \texttt{PI} & $\pi$ & 3.1415926535898 & 1 \\
      \texttt{TWOPI} & $2 \pi$ & 6.2831853071796 & 1 \\
      \texttt{RADDEG} & $180/\pi$ & 57.295779513082 & rad/deg \\
      \texttt{DEGRAD} & $\pi/180$ & .017453292519943 & deg/rad \\
      \texttt{E} & $e$ & 2.7182818284590 & 1 \\
      \texttt{EMASS} & $m\_e$ & .51099906e-3 & GeV \\
      \texttt{PMASS} & $m\_p$ & .93827231 & GeV \\
      \texttt{HMMASS} &  $m\_{h^{-}}$ & .939277 & GeV \\
      \texttt{CMASS} & $m_c$ & 12*0.931494027 & GeV \\
      \texttt{UMASS} & $m_u$ & 238*0.931494027 & GeV \\
      \texttt{MMASS} & $m_\mu$ & 0.1057 & GeV \\
      \texttt{DMASS} & $m_d$ & 2*0.931494027 & GeV \\
      \texttt{XEMASS} & $m_{xe}$ &124*0.931494027 & GeV \\
      \texttt{CLIGHT} & $c$ & 299792458 & m/s \\
      \textit{OPALVERSION} &  & 120 & for 1.2.0 \\
      \texttt{RANK} & & $0\ldots N_{p}-1$ & 1 \\
      \hline
    \end{tabular}
  \end{center}
\end{table}
Here the \texttt{RANK} represents the MPI-Rank of the process and $N_{p}$ the total number of MPI processes.
\subsection{Variable labels}
\label{sec:avariable}
\index{Variable}
Often a set of numerical values depends
on a common variable parameter.
Such a variable must be defined as a global
variable see~Section~\ref{variable} defined by one of
\begin{verbatim}
REAL X=expression;
REAL X:=expression;
VECTOR X=vector-expression;
VECTOR X:=vector-expression;
\end{verbatim}
When such a variable is used in an expression,
\textit{OPAL} uses the current value of the variable.
When the value is a constant or an expression preceded by the delimiter
\texttt{=} it is evaluated immediately and the result is assigned to the
variable as a constant.
When the value is an expression preceded by the delimiter \texttt{:=}
the expression is retained and re-evaluated whenever one of its operands
changes.\\
\noindent Example:
\begin{verbatim}
REAL L=1.0;
REAL X:=L;
D1:DRIFT,L:=X;
D2:DRIFT,L:=2.0-X;
\end{verbatim}
When the value of \texttt{X} is changed,
the lengths of the drift spaces are recalculated as
\texttt{X} and \texttt{2-X} respectively.

\subsection{Element or command attributes}
In arithmetic expressions the attributes of physical elements
or commands can occur as operands.
They are named respectively by
\begin{verbatim}
element-name->attribute-name
command-name->attribute-name
\end{verbatim}
If they are arrays, they are denoted by
\begin{verbatim}
element-name->attribute-name[index]
command-name->attribute-name[index]
\end{verbatim}
Values are assigned to attributes in element definitions or commands.

\noindent Example:
\begin{verbatim}
D1:DRIFT,L=1.0;
D2:DRIFT,L=2.0-D1->L;
\end{verbatim}
\texttt{D1->L} refers to the length \texttt{L} of the drift space \texttt{D1}.

\subsection{Deferred Expressions and Random Values}
\label{sec:adefer}
Definition of random machine imperfections requires evaluation
of expressions containing random functions.
These are not evaluated like other expressions before a command
begins execution, but sampled as required from the distributions
indicated when errors are generated.
Such an expression is known as a \textbf{deferred expression}.
Its value cannot occur as an operand in another expression.

\section{Element Selection}
\index{Selection Of Elements}
\index{Element!Selection}
\index{Place}
Many \textit{OPAL} commands allow for the possibility to process or display
a subset of the elements occurring in a beam line or sequence. This is not yet available in:
\texttt{DOPAL-t} and \texttt{DOPAL-cycl}.

\subsection{Element Selection}
\label{sec:aplace}
A \texttt{place} denotes a single element, or the position
\textbf{following} that element.
It can be specified by one of the choices
\begin{description}
\item[{object-name[index]}]
The name \verb'object-name' is the name of an element, line or sequence,
and the integer \texttt{index} is its occurrence count in the beam line.
If the element is unique, \texttt{[index]} can be omitted.
\item[{\#S}]
denotes the position before the first physical element in the \textbf{full}
beam line.
This position can also be written \texttt{\#0}.
\item[{\#E}]
denotes the position after the last physical element in the \textbf{full}
beam line.
\end{description}
Either form may be qualified by one or more beam line names,
as described by the formal syntax:
\begin{verbatim}
place ::= element-name |
          element-name "[" integer "]" |
          "#S" |
          "#E" |
          line-name "::" place
\end{verbatim}
An omitted index defaults to one.
\noindent Examples: assume the following definitions:
\begin{verbatim}
M: MARKER;
S: LINE=(C,M,D);
L: LINE=(A,M,B,2*S,A,M,B);
   SURVEY,LINE=L
\end{verbatim}
The line \texttt{L} is equivalent to the sequence of elements
\begin{verbatim}
A,M,B,C,M,D,C,M,D,A,M,B
\end{verbatim}
Some possible \texttt{place} definitions are:
\begin{description}
\item[{C[1]}]
The first occurrence of element \texttt{C}.
\item[\#S]
The beginning of the line \texttt{L}.
\item[{M[2]}]
The second marker \texttt{M} at top level of line \texttt{L},
i.~e. the marker between second \texttt{A} and the second \texttt{B}.
\item[\#E]
The end of line \texttt{L}
\item[{S[1]::M[1]}]
The marker \texttt{M} nested in the first occurrence of \texttt{S}.
\end{description}

\subsection{Range Selection}
\label{sec:arange}
\index{Element!Range}
\index{Range}
A \texttt{range} in a beam line see~Section~\ref{line} is selected
by the following syntax:
\begin{verbatim}
range ::= place |
          place "/" place
\end{verbatim}
This denotes the range of elements from the first\texttt{place} to
the second \texttt{place}. Both positions are included.
A few special cases are worth noting:
\begin{itemize}
\item
When \texttt{place1} refers to a \texttt{LINE} see~Section~\ref{line}.
the range starts at the \textbf{beginning} of this line.
\item
When \texttt{place2} refers to a \texttt{LINE} see~Section~\ref{line}.
the range ends at the \textbf{ending} of this line.
\item
When both \texttt{place} specifications refer to the same object,
then the second can be omitted.
In this case, and if \texttt{place} refers to a
\texttt{LINE} see~Section~\ref{line} the range contains the whole of the line.
\end{itemize}
\noindent Examples: Assume the following definitions:
\begin{verbatim}
M: MARKER;
S: LINE=(C,M,D);
L: LINE=(A,M,B,2*S,A,M,B);
\end{verbatim}
The line L is equivalent to the sequence of elements
\begin{verbatim}
A,M,B,C,M,D,C,M,D,A,M,B
\end{verbatim}
\noindent Examples for \texttt{range} selections:
\begin{description}
\item[\#S/\#E]
  The full range or \texttt{L}.
\item[{A[1]/A[2]}]
  \texttt{A[1]} through \texttt{A[2]}, both included.
\item[{S::M/S[2]::M}]
  From the marker \texttt{M} nested in the first occurrence of
  \texttt{S},
  to the marker \texttt{M} nested in the second occurrence of
  \texttt{S}.
\item[{S[1]/S[2]}]
  Entrance of first occurrence of \texttt{S} through
  exit of second occurrence of \texttt{S}.
\end{description}

\section{Constraints}
\label{sec:aconstraint}
\index{Constraint}
Please note this is not yet available in:
\texttt{DOPAL-t} and \texttt{DOPAL-cycl}.

In matching it is desired to specify equality constraints,
as well as lower and upper limits for a quantity.
\textit{OPAL} accepts the following form of constraints:
\begin{verbatim}
constraint          ::= array-expr constraint-operator array-expr

constraint-operator ::= "==" | "<" | ">"
\end{verbatim}

\section{Variable Names}
\label{sec:areference}
\index{Variable}
A variable name can have one of the formats:
\begin{verbatim}
   variable name ::= real variable |
                     object"->"real attribute
\end{verbatim}
The first format refers to the value of the
{global variable} see~Section~\ref{variable},
the second format refers to a named \texttt{attribute} of the named
\texttt{object}.
\texttt{object} can refer to an element or a command

\section{Regular Expressions}
\label{sec:wildcard}
\index{Regular Expression}
\index{Wildcard}
Some commands allow selection of items via a \texttt{regular-expression}.
Such a pattern string \textbf{must} be enclosed in single or double quotes;
and the case of letters is significant.
The meaning of special characters follows the standard UNIX usage:

\begin{description}
\item[.]
Stands for a single arbitrary character,
\item[{[letter...letter]}]
Stands for a single character occurring in the bracketed string,
\noindent Example: ``\texttt{[abc]}'' denotes the choice of one of
\texttt{a,b,c}.
\item[{[character-character]}]
Stands for a single character from a range of characters,
\noindent Example: ``\texttt{[a-zA-Z]}'' denotes the choice of any letter.
\item[*]
Allows zero or more repetitions of the preceding item,
\noindent Example: ``\texttt{[A-Z]*}'' denotes a string of zero or more
upper case letters.
\item[$\backslash$character]
Removes the special meaning of \texttt{character},
\noindent Example: ``\texttt{$\backslash$*}'' denotes a literal asterisk.
\end{description}
All other characters stand for themselves.
The pattern
\begin{verbatim}
"[A-Za-z][A-Za-z0-9_']*"
\end{verbatim}
illustrates all possible unquoted identifier formats see~Section~\ref{label}.
Since identifiers are converted to lower case,
after reading they will match the pattern
\begin{verbatim}
"[a-z][a-z0-9_']*"
\end{verbatim}
\noindent Examples for pattern use:
\begin{verbatim}
SELECT,PATTERN="D.."
SAVE,PATTERN="K.*QD.*\.R1"
\end{verbatim}
The first command selects all elements whose names have exactly three
characters and begin with the letter \texttt{D}.
The second command saves definitions beginning with the letter \texttt{K},
containing the string \texttt{QD}, and ending with the string \texttt{.R1}.
The two occurrences of \verb'.*' each stand for an arbitrary
number (including zero) of any character,
and the occurrence \verb'\.' stands for a literal period.

\section{Arrays}
\label{sec:anarray}
\index{Real!Array}
\index{Array!Real}
An attribute array is a set of values of the same
{attribute type} see~Section~\ref{attribute}.
Normally an array is entered as a list in braces:
\begin{verbatim}
{value,...,value}
\end{verbatim}
The list length is only limited by the available storage.
If the array has only one value, the braces (\texttt{{}}) can be omitted:
\begin{verbatim}
value
\end{verbatim}

\subsection{Logical Arrays}
\label{sec:logarray}
\index{Array!Logical}
\index{Logical!Array}
For the time being, logical arrays can only be given as a list.
The formal syntax is:
\begin{verbatim}
logical-array ::= "{" logical-list "}"

logical-list  ::= logical-expr |
                  logical-list "," logical-expr
\end{verbatim}
\par
\noindent Example:
\begin{verbatim}
{true,true,a==b,false,x>y && y>z,true,false}
\end{verbatim}

\subsection{Real Arrays}
\label{sec:realarray}
\index{Array!Real}
\index{Real!Array}
Real arrays have the following syntax:
\begin{footnotesize}
\begin{verbatim}
array-ref     ::= array-variable |
                  object "->" array-attribute |

table-ref     ::= "ROW" "(" table "," place ")" |
                  "ROW" "(" table "," place "," column-list ")"
                  "COLUMN" "(" table "," column ")" |
                  "COLUMN" "(" table "," column "," range ")"

columns       ::= column |
                  "{" column-list "}"

column-list   ::= column |
                  column-list "," column

column        ::= string

real-list     ::= real-expr |
                  real-list "," real-expr

index-select  ::= integer |
                  integer "," integer |
                  integer "," integer "," integer

array-primary ::= "{" real-list "}" |
                  "TABLE" "(" index-select "," real-expr ")" |
                  array-ref |
                  table-ref |
                  array-function-name "(" arguments ")" |
                  (array-expression)

array-factor  ::= array-primary |
                  array-factor "^" array-primary

array-term    ::= array-factor |
                  array-term "*" array-factor |
                  array-term "/" array-factor

array-expr    ::= array-term |
                  "+" array-term |
                  "-" array-term |
                  array-expr "+" array-term |
                  array-expr "-" array-term |
\end{verbatim}
\end{footnotesize}

\begin{table}[!htb] \footnotesize
  \begin{center}
    \caption{Real Array Functions in \textit{OPAL} (acting component-wise)}
    \label{tab:compfun}
    \begin{tabular}{|l|p{0.5\textwidth}|l|l|}
      \hline
      \tabhead Function & Meaning & result type & argument type \\
      \hline
      \texttt{TRUNC(X)} &
      truncate \texttt{X} towards zero (discard fractional part) &
      real array &real array \\
      \texttt{ROUND(X)} & round \texttt{X} to nearest integer &
      real array &real array \\
      \texttt{FLOOR(X)} & return largest integer not greater than \texttt{X} &
      real array &real array \\
      \texttt{CEIL(X)} & return smallest integer not less than \texttt{X} &
      real array &real array \\
      \texttt{SIGN(X)} & return sign of \texttt{X}
      (+1 for \texttt{X} positive, -1 for \texttt{X} negative,
      0 for \texttt{X} zero) & real array &real array \\
      \texttt{SQRT(X)} & return square root of \texttt{X} &
      real array &real array \\
      \texttt{LOG(X)} & return natural logarithm of \texttt{X} &
      real array &real array \\
      \texttt{EXP(X)} & return exponential to the base $e$ of \texttt{X} &
      real array &real array \\
      \texttt{SIN(X)} & return trigonometric sine of \texttt{X} &
      real array &real array \\
      \texttt{COS(X)} & return trigonometric cosine of \texttt{X} &
      real array &real array \\
      \texttt{ABS(X)} & return absolute value of \texttt{X} &
      real array &real array \\
      \texttt{TAN(X)} & return trigonometric tangent of \texttt{X} &
      real array &real array \\
      \texttt{ASIN(X)} & return inverse trigonometric sine of \texttt{X} &
      real array &real array \\
      \texttt{ACOS(X)} & return inverse trigonometric cosine of \texttt{X} &
      real array &real array \\
      \texttt{ATAN(X)} & return inverse trigonometric tangent of \texttt{X} &
      real array &real array \\
      \texttt{TGAUSS(X)} &
      random number, Gaussian distribution with $\sigma$=1,
      truncated at \texttt{X} &
      real array &real array \\
      \texttt{USER1(X)} &
      random number, user-defined distribution with one parameter &
      real array &real array \\
      \texttt{EVAL(X)} &
      evaluate the argument immediately and transmit it as a constant &
      real array &real array \\
      \hline
    \end{tabular}
  \end{center}
\end{table}
\noindent Example:
\begin{verbatim}
{a,a+b,a+2*b}
\end{verbatim}
There are also three functions allowing the generation of real arrays:
\begin{description}
\item[TABLE]
  \index{TABLE}
  Generate an array of expressions:
\begin{verbatim}
TABLE(n2,expression)    // implies
                        // TABLE(1:n2:1,expression)
TABLE(n1:n2,expression) // implies
                        // TABLE(n1:n2:1,expression)
TABLE(n1:n2:n3,expression)
\end{verbatim}
  These expressions all generate an array with \texttt{n2} components.
  The components selected by \texttt{n1:n2:n3} are filled from the given
  \texttt{expression};
  a C pseudo-code for filling is
\begin{verbatim}
int i;
for (i = n1; i <= n2; i += n3) a[i] = expression(i);
\end{verbatim}
  In each generated expression the special character hash sign (\texttt{\#})
  is replaced by the current value of the index \texttt{i}.
  \par
  \noindent Example:
\begin{verbatim}
// an array with 9 components, evaluates to
// {1,4,7,10,13}:
table(5:9:2,3*#+1) // equivalent to
                   // {0,0,0,0,16,0,22,0,28}
\end{verbatim}
\item[ROW]
  \index{ROW}
  Generate a table row:
\begin{verbatim}
ROW(table,place)             // implies all columns
ROW(table,place,column list)
\end{verbatim}
  This generates an array containing the named (or all) columns in the
  selected place.
\item[COLUMN]
  \index{COLUMN}
  Generate a table column:
\begin{verbatim}
COLUMN(table,column)         // implies all rows
COLUMN(table,column,range)
\end{verbatim}
  This generates an array containing the selected (or all) rows of the
  named column.
\end{description}

\subsection{String Arrays}
\label{sec:strarray}
\index{Array!String}
\index{String!Array}
String arrays can only be given as lists of single values.
For permissible values String values see~Section~\ref{astring}.
\par
\noindent Example:
\begin{verbatim}
{A, "xyz", A & STRING(X)}
\end{verbatim}

\subsection{Token List Arrays}
\label{sec:tokarray}
\index{Array!Token List}
\index{Token List!Array}
Token list arrays are always lists of single token lists.
\par
\noindent Example:
\begin{verbatim}
{X:12:8,Y:12:8}
\end{verbatim}
