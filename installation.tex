\input{header}

\chapter{Build and Installation}
\label{chp:installation}
\textit{OPAL} and all its flavors are based on several packages which are all installable using \texttt{cmake} or the configure-make-install trilogy.

\textit{OPAL} is also preinstalled on several HPC clusters including the Merlin cluster at PSI. The preinstalled version can be accessed
using the module command:
\begin{footnotesize}
\begin{verbatim}
module load OPAL/1.6.0
\end{verbatim}
\end{footnotesize}

\section{Using pre-build Binaries}
Pre-build binaries are available for SL Linux  and Mac OS X at the following download page: \url{https://amas.psi.ch/OPAL/wiki/Downloads}.

\section{Enabling the Iterative Solvers for Space-Charge Calculation}

Iterative solvers are used to solve the Poisson's equation for the electrostatic potential
on three-dimensional domains \ref{Adelmann:2009p543}.
The multigrid preconditioner and iterative solver are implemented using Trilinos framework. It is essential to install
Trilinos software framework to use the iterative solvers in \textit{OPAL}.

Trilinos is also preinstalled on several HPC clusters including the Merlin cluster at PSI. The preinstalled version can be accessed
using the module command:
\begin{footnotesize}
\begin{verbatim}
module load trilinos
\end{verbatim}
\end{footnotesize}

{\bf Please note:} The Multigrid space charge solver is not yet capable of emitting a beam from the cathode.  You are advised to use the FFT space charge solver for studies with particle emission.

\subsubsection{Trilinos Package}
If Trilinos version ($>$11.0) is not available on your system, download it from the Trilinos webpage
{\url {http://trilinos.sandia.gov/download}}
\footnote{\url{http://trilinos.sandia.gov}}. CMake build system is used to configure, build and install
Trilinos. Instructions for building Trilinos is available in \\
{\url {http://trilinos.sandia.gov/TrilinosBuildQuickRef.html}}

To build {\textit{OPAL}} with {SAAMG\_SOLVER}, Trilinos packages, which are needed to be enabled are:
\begin{itemize}
  \item epetra and epetraext
  \item ml and ml\_parmetis3x
  \item amesos and amesos-superludist
  \item ifpack
  \item teuchos and teuchos-extended
  \item aztecco and aztecoo-teuchos
  \item galeri
  \item belos
\end{itemize}
and Third-Party Library (TPL) support
 \begin{itemize}
  \item ParMETIS (\texttt{-D TPL\_ENABLE\_ParMETIS:BOOL=ON})
  \item BLAS  (\texttt{-D TPL\_ENABLE\_BLAS:BOOL=ON})
  \item LAPACK (\texttt{-D TPL\_ENABLE\_LAPACK:BOOL=ON})
  \item MPI (\texttt{-D TPL\_ENABLE\_MPI:BOOL=ON })
\end{itemize}
To enable a given TPL,  the path to this package's header include and library directories should be specified
when building Trilinos. The TPL libraries such as \textit{libparmetis.a libmetis.a libblas.a liblapacke.a liblapack.a} are needed
to use MultiGrid (SAAMG).

\subsection{Environment Variables}
\label{ssec:envvar_MG}

Assuming Trilinos package is installed in \texttt{\$TRILINOS\_PREFIX} then the following environment variables must be set accordingly.

\texttt{TRILINOS\_DIR} should point the path to the base installation directory of Trilinos

\texttt{TRILINOS\_INCLUDE\_DIR} should point the path to Trilinos' include directory

\texttt{TRILINOS\_LIBRARY\_DIR} should point the path to Trilinos' library directory

If you are using preinstalled version of Trilinos on Merlin cluster at PSI, the environment variables
are automatically set so you don't have to set these yourself.

\begin{footnotesize}
\begin{verbatim}
export TRILINOS_DIR=$TRILINOS_PREFIX
export TRILINOS_INCLUDE_DIR=$TRILINOS_DIR/include
export TRILINOS_LIBRARY_DIR=$TRILINOS_DIR/lib
\end{verbatim}
\end{footnotesize}

\subsection{Build \textit{OPAL} with SAAMG\_SOLVER enabled}
You can enable the solver with\\ \texttt{-DENABLE\_SAAMG\_SOLVER=TRUE} using cmake
and make sure the environment variables Section~\ref{envvar_MG}
are set up already before {\textit{OPAL}} installation.

\begin{footnotesize}
\begin{verbatim}
cd $OPAL_ROOT
mkdir build
cd build
cmake -DENABLE_SAAMG_SOLVER=TRUE $OPAL_ROOT
make
\end{verbatim}
\end{footnotesize}

\subsection{Build \textit{OPAL} with AMR enabled}
{\bf AMReX} \footnote{\url{https://ccse.lbl.gov/AMReX}} software framework is used in \textit{OPAL}\  to add the adaptive mesh
refinement (AMR) technique. You can enable AMR solver with {\tt -DENABLE\_AMR=TRUE} using cmake.

\begin{footnotesize}
\begin{verbatim}
cd $OPAL_ROOT
mkdir build
cd build
cmake -DENABLE_AMR=TRUE $OPAL_ROOT
make
\end{verbatim}
\end{footnotesize}
Please make sure one of the following environment variables is set in order to find the AMReX configuration file
\verb|AMReXConfig.cmake|:
\begin{footnotesize}
\begin{verbatim}
export AMREX_HOME=/path/to/target/directory
export AMREX_PREFIX=$AMREX_HOME
export AMREX_DIR=$AMREX_HOME
\end{verbatim}
 \label{subsec:envvar_AMR}
\end{footnotesize}

\subsubsection{AMReX Package}
\label{ssubsection:AMRexBuild}

You can download AMReX from the GitHub server by typing the following commands
\begin{footnotesize}
\begin{verbatim}
cd $HOME/git
git clone https://github.com/AMReX-Codes/amrex.git
\end{verbatim}
\end{footnotesize}
AMReX is Fortran based with extensions to C++.\ It runs in parallel using MPI.\ Please check the following environment variable
before start building the library.
\begin{verbatim}
export AMREX_INSTALL_PREFIX=/path/to/target/directory
\end{verbatim}
Build and install as follows:
\begin{footnotesize}
\begin{verbatim}
cd amrex
mkdir build
cd build
CXX=$MPICXX CC=$MPICC cmake -DENABLE_PARTICLES=1
                            -DENABLE_MG_BOXLIB=1 
                            -DCMAKE_BUILD_TYPE=Release
                            -DCMAKE_INSTALL_PREFIX=$AMREX_INSTALL_PREFIX ..
make -j4
make install    // depending on target directory root access might be required
\end{verbatim}
\end{footnotesize}
Now you are ready to build \textit{OPAL}\ with AMR support.

\section{Debug Flags}\label{sec:debugflags}

\begin{table}[ht]\footnotesize
  \begin{center}
    \caption{Debug flags.}
    \label{tbl:debug_flags}
      \begin{tabular}{lll}
        \hline
        \tabhead Name & Description & Default \\
        \hline
        DBG\_SCALARFIELD & dumps scalar potential on the grid & not set \\
        DBG\_STENCIL & dumps stencil (SAAMG solver) to a Matlab readable file & not set \\
        \hline
      \end{tabular}
    \end{center}
\end{table}

\paragraph{DBG\_SCALARFIELD} dumps the field to a file called rho\_scalar. The structure of the data can be deduced from the following Matlab script:

\begin{footnotesize}
\begin{verbatim}
function scalfield(RHO)

rhosize=size(RHO)
for i=1:rhosize(1)
  x = RHO(i,1);
  y = RHO(i,2);
  z = RHO(i,3);
  rhoyz(y,z) = RHO(i,4);
  rhoxy(x,y) = RHO(i,4);
  rhoxz(x,z) = RHO(i,4);
  rho(x,y,z) = RHO(i,4);
end
\end{verbatim}
\end{footnotesize}

\paragraph{DBG\_STENCIL} dumps the discretization stencil to a file (A.dat). The following Matlab code will read and store the sparse matrix in the variable 'A'.

\begin{footnotesize}
\begin{verbatim}
load A.dat;
A = spconvert(A);
\end{verbatim}
\end{footnotesize}

\section{\textit{OPAL}~as a Library}
An \textit{OPAL}~library can be build by specifying \texttt{-DBUILD\_LIBOPAL} in the cmake process. The \textit{OPAL}~library is currently used in the opt-pilot, a multi-objective
optimization package \ref{bib:optpilot1}.

\section{Emacs Mode for \textit{OPAL}}
An opal-mode for emacs is provided to get highlighted input files. To use it the user should make the directory \textit{\$HOME/.emacs.d/opal} and copy the file \textit{opal.el} there. Then the following lines
\begin{footnotesize}
\begin{verbatim}
(add-to-list 'load-path "~/.emacs.d/opal")
(autoload 'opal-mode "opal.el" "Enter opal mode" t)
(setq auto-mode-alist (append '(("\\.opal\$" . opal-mode)) auto-mode-alist))
(setq auto-mode-alist (append '(("\\.in\$" . opal-mode)) auto-mode-alist))
\end{verbatim}
\end{footnotesize}
should be added to the emacs configuration file \textit{\$HOME/.emacs}. In case your input file has the extensions \textit{.opal} or \textit{.in}  you will enjoy highlighted
keywords, constants etc.

\section{Regression tests}
The regression rest repository can be found at \url{git@gitlab.psi.ch:OPAL/regression-tests.git}. Several test can be found which are
run on every source code change in order to check the validity of the current version of \textit{OPAL}. This is a good starting-point to learn how to
model accelerators with the various flavours of \textit{OPAL}.

\section{Build unit tests} \label{chp:unittest}
Some portions of \textit{OPAL} can be tested using \emph{unit tests}. Unit tests are implemented to ensure that a particular unit e.g. a function or class works as intended. \textit{OPAL} uses the google testing \emph{gtest} framework to support unit tests. Documentation can be found at \url{https://code.google.com/p/googletest/w/list}.

The \textit{OPAL} unit tests are by default not built. To build the unit tests, first gtest must be installed.

\begin{verbatim}
cd ${OPAL_ROOT}/tests/tools
bash install_gtest.bash
\end{verbatim}

This will check that you have the required packages to build gtest, download the gtest source code from the google website and attempt to build it.

Next modify the cmake files to flag that the unit tests need to be built.
\begin{verbatim}
cd ${OPAL_BUILD_ROOT}/
cmake -DBUILD_OPAL_UNIT_TESTS=1
make
\end{verbatim}
If all goes well, the unit tests should build. To execute the tests, do
\begin{verbatim}
cd ${OPAL_BUILD_ROOT}/
./tests/opal_unit_tests
\end{verbatim}
You should get output that looks like e.g.
\begin{verbatim}
[==========] Running 47 tests from 8 test cases.
[----------] Global test environment set-up.
[----------] 6 tests from OPALRingSectionTest
[ RUN      ] OPALRingSectionTest.TestConstructDestruct
[       OK ] OPALRingSectionTest.TestConstructDestruct (0 ms)
<snip>
[ RUN      ] PolynomialTimeDependenceTest.TDMapNameLookupTest
[       OK ] PolynomialTimeDependenceTest.TDMapNameLookupTest (0 ms)
[----------] 3 tests from PolynomialTimeDependenceTest (0 ms total)

[----------] Global test environment tear-down
[==========] 47 tests from 8 test cases ran. (25 ms total)
[  PASSED  ] 47 tests.

  YOU HAVE 1 DISABLED TEST
\end{verbatim}
If the output does not end in PASSED then something failed. Review the test output to find the test failure.

\subsubsection{Adding a unit test}
To add a unit test to the \textit{OPAL} framework, either edit an existing file or add a new file. In general, there should be one unit test per pair of header, source files. The directory structure reflects \textit{OPAL} directory structure. Source code that is in \verb|${OPAL_ROOT}/classic/5.0/src/<dir>/<Filename.h,cpp>| should have corresponding unit test files in \verb|${OPAL_ROOT}/src/unit_tests/classic_src/<dir>/<FilenameTest.cpp>|. Source code that is in \verb|${OPAL_ROOT}/src/<dir>/<Filename.h,cpp>| should have corresponding unit test files in \verb|${OPAL_ROOT}/src/unit_tests/opal_src/<dir>/<FilenameTest.cpp>|. To update the list of files used by cmake, do
\begin{verbatim}
touch ${OPAL_ROOT}/CMakeLists.txt
\end{verbatim}
Then run \verb|make| as usual.

\input{footer}